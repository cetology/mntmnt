FROM centos:7

RUN yum install python3 python3-pip -y
RUN pip3 install flask flash_restful

COPY api.py /opt/api/api.py
CMD ["python3", "/opt/api/api.py"]
